﻿using NUnit.Framework;
using TT.Domain.Procedures;

namespace TT.Tests.Services
{
    class PlayerProcedureTests
    {

        [TestFixture]
        public class Tests_for_base_willpower_mana_and_xp_to_next_level
        {

            // privates

            [SetUp]
            public void SetUp()
            {

            }

            [Test]
            public void Should_retrieve_correct_xp_requirement_for_levelup()
            {
                var xpRequired_lvl1 = PlayerProcedures.GetXPNeededForLevelUp(1);
                Assert.AreEqual(100, xpRequired_lvl1);

                var xpRequired_lvl2 = PlayerProcedures.GetXPNeededForLevelUp(2);
                Assert.AreEqual(140, xpRequired_lvl2);

                var xpRequired_lvl3 = PlayerProcedures.GetXPNeededForLevelUp(3);
                Assert.AreEqual(200, xpRequired_lvl3);

                var xpRequired_lvl12 = PlayerProcedures.GetXPNeededForLevelUp(12);
                Assert.AreEqual(1680, xpRequired_lvl12);

                var xpRequired_lvl27 = PlayerProcedures.GetXPNeededForLevelUp(27);
                Assert.AreEqual(8120, xpRequired_lvl27);

            }

            [Test]
            public void Should_retrieve_correct_mana_base_by_level()
            {
                var manaBase_lvl1 = PlayerProcedures.GetManaBaseByLevel(1);
                Assert.AreEqual(50, manaBase_lvl1);

                var manaBase_lvl2 = PlayerProcedures.GetManaBaseByLevel(2);
                Assert.AreEqual(55, manaBase_lvl2);

                var manaBase_lvl7 = PlayerProcedures.GetManaBaseByLevel(7);
                Assert.AreEqual(80, manaBase_lvl7);

                var manaBase_lvl55 = PlayerProcedures.GetManaBaseByLevel(55);
                Assert.AreEqual(320, manaBase_lvl55);
            }


            [Test]
            public void Should_retrieve_correct_wp_base_by_level()
            {
                var wpBase_lvl1 = PlayerProcedures.GetWillpowerBaseByLevel(1);
                Assert.AreEqual(100, wpBase_lvl1);

                var wpBase_lvl2 = PlayerProcedures.GetWillpowerBaseByLevel(2);
                Assert.AreEqual(110, wpBase_lvl2);

                var wpBase_lvl7 = PlayerProcedures.GetWillpowerBaseByLevel(7);
                Assert.AreEqual(160, wpBase_lvl7);

                var wpBase_lvl55 = PlayerProcedures.GetWillpowerBaseByLevel(55);
                Assert.AreEqual(640, wpBase_lvl55);
            }
        }

    }
}
