﻿namespace TT.Domain.Items.DTOs
{
    public class ItemSourceNameDescription
    {
        public string FriendlyName { get; set; }
        public string Description { get; set; }
        public string PortraitUrl { get; set; }
    }

}
